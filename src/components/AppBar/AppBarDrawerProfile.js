import { useTranslation } from 'react-i18next'

import { Avatar, Box, Button } from '@material-ui/core'

import { stylesDrawerAvatar } from './styles'
import { useDisclosure } from '../../hooks/useDisclosure'
import { AppBarDrawerProfileDialog } from './AppBarDrawerProfileDialog'

export const AppBarDrawerProfile = () => {
  const { t } = useTranslation()
  const dialogEdit = useDisclosure()

  const handleClickEdit = (id) => {
    dialogEdit.onOpen()
  }
  const classes = stylesDrawerAvatar()

  return (
    <>
      <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" marginY={5}>
        <Box marginBottom={2}>
          <Avatar className={classes.avatar}>JJ</Avatar>
        </Box>

        <Box fontSize={18} fontWeight="fontWeightBold" marginBottom={4}>
          Johannes Justesen
        </Box>

        <Box width={1} paddingX={2}>
          <Button onClick={() => handleClickEdit()} variant="contained" color="primary" fullWidth>
            {t('myProfile')}
          </Button>
        </Box>
        {dialogEdit.isOpen && (
          <AppBarDrawerProfileDialog
            opened={dialogEdit.isOpen}
            onClose={dialogEdit.onClose}
            id={dialogEdit.id}
          ></AppBarDrawerProfileDialog>
        )}
      </Box>
    </>
  )
}
