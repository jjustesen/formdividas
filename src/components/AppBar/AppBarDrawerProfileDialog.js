import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { CardListInfo } from '../CardListInfo'

export const AppBarDrawerProfileDialog = ({ opened, onClose, id }) => {
  const { t } = useTranslation()

  const user = {
    name: 'Johannes Leandro Laeber Justesen',
    phone: '(53) 99922-6613',
    email: 'johannesjustensen99@gmail.com',
    website: 'gitlab.com/jjustesen'
  }

  return (
    <Dialog open={opened} onClose={onClose} aria-labelledby="form-dialog-title" fullWidth>
      <DialogTitle id="form-dialog-title">{t('Infos')}</DialogTitle>
      <DialogContent>
        <CardListInfo user={user} />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary" type="submit">
          {t('confirm')}
        </Button>
      </DialogActions>
    </Dialog>
  )
}
