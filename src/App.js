import React, { lazy, Suspense, useMemo } from 'react'

import { CircularProgress } from '@material-ui/core'
const Admin = lazy(() => import('./pages/Admin'))

function App() {
  const Component = useMemo(() => {
    return Admin
  }, [])

  return (
    <Suspense fallback={<CircularProgress />}>
      <Component />
    </Suspense>
  )
}

export default App
