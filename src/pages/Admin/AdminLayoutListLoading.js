import React, { memo } from 'react'

import { Box, Divider } from '@material-ui/core'

import { AdminLayoutListItemLoading } from './AdminLayoutListItemLoading'

export const AdminLayoutListLoading = memo(() => {
  return (
    <Box>
      {Array.from({ length: 3 }).map(() => (
        <>
          <AdminLayoutListItemLoading />

          <Box my={3}>
            <Divider />
          </Box>
        </>
      ))}
    </Box>
  )
})
