import React, { memo } from 'react'

import { IconButton, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'

import { formatDate } from '../../helpers/dates'

import { useUser } from '../../helpers/useUser'
import { useTranslation } from 'react-i18next'
import { CardListInfo } from '../../components/CardListInfo'
import { Delete, Edit } from '@material-ui/icons'
import { DialogDividaEdit } from '../Dialog/DialogDividaEdit'
import { useQueryClient } from 'react-query'
import { useDisclosure } from '../../hooks/useDisclosure'
import { DialogDividaDelete } from '../Dialog/DialogDividaDelete'

export const AdminLayoutListItem = memo(({ ...props }) => {
  const user = useUser(props.value[0].idUsuario)
  const { t } = useTranslation()
  const queryClient = useQueryClient()

  const dialogEdit = useDisclosure()
  const dialogDelete = useDisclosure()

  const handleClickEdit = (id) => {
    dialogEdit.onOpen({
      id,
      onCancel: () => {
        dialogEdit.onClose()
        queryClient.cancelQueries('getDividasById')
      }
    })
  }

  const handleClickDelete = (id) => {
    dialogDelete.onOpen({
      id,
      onCancel: () => {
        dialogDelete.onClose()
        queryClient.cancelQueries('getDividasById')
      }
    })
  }

  return (
    <>
      <CardListInfo user={user} />

      <TableContainer component={Paper}>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell> {t('reason')}</TableCell>
              <TableCell align="center"> {t('value')}</TableCell>
              <TableCell align="center"> {t('date')}</TableCell>
              <TableCell align="center"> </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.value.map((divida) => {
              return (
                <TableRow key={divida.name}>
                  <TableCell component="th" scope="row">
                    {divida.motivo}
                  </TableCell>
                  <TableCell align="center">
                    {divida.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}
                  </TableCell>
                  <TableCell align="center">{formatDate(divida.criado)}</TableCell>

                  <TableCell align="right">
                    <IconButton
                      onClick={() => handleClickDelete(divida._id)}
                      aria-label="upload picture"
                      component="span"
                    >
                      <Delete />
                    </IconButton>
                    <IconButton
                      onClick={() => handleClickEdit(divida._id)}
                      aria-label="upload picture"
                      component="span"
                    >
                      <Edit />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
      {dialogEdit.isOpen && (
        <DialogDividaEdit
          opened={dialogEdit.isOpen}
          onClose={dialogEdit.onClose}
          onCancel={dialogEdit.onCancel}
          id={dialogEdit.id}
        ></DialogDividaEdit>
      )}
      {dialogDelete.isOpen && (
        <DialogDividaDelete
          opened={dialogDelete.isOpen}
          onClose={dialogDelete.onClose}
          onCancel={dialogDelete.onCancel}
          id={dialogDelete.id}
        ></DialogDividaDelete>
      )}
    </>
  )
})
