import React, { useMemo } from 'react'

import {
  Avatar,
  Box,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
  useTheme
} from '@material-ui/core'

import PersonIcon from '@material-ui/icons/Person'
import { useTranslation } from 'react-i18next'
import { useQueryGetDividas } from '../../api/queries/getDividas'
import { groupBy } from '../../helpers/groupBy'
import { useQueryGetClientes } from '../../api/queries/getClientes'
import { usePerson } from '../../providers/person'

export const AdminLayoutPeople = () => {
  const theme = useTheme()
  const { setPerson } = usePerson()

  const { t } = useTranslation()

  const boxBorderRight = useMemo(() => {
    return `1px solid ${theme.palette.divider}`
  }, [theme.palette.divider])

  const {
    data: { result }
  } = useQueryGetDividas()
  const { data: dataUser } = useQueryGetClientes()

  const dataPessoas = groupBy(result, (user) => user.idUsuario)

  const pessoas = dataUser.filter((user) => dataPessoas.find((a) => a.value[0].idUsuario === user.id))

  return (
    <Box width={400} paddingX={3} paddingY={4} borderRight={boxBorderRight}>
      <Box>
        <Typography variant="h5" gutterBottom>
          {t('people')}
        </Typography>
        <Divider />
      </Box>
      <List>
        <ListItem button onClick={() => setPerson(null)}>
          <ListItemAvatar>
            <Avatar>
              <PersonIcon />
            </Avatar>
          </ListItemAvatar>

          <ListItemText>{t('all')}</ListItemText>
        </ListItem>
        {pessoas.map((pessoa) => (
          <ListItem button onClick={() => setPerson(pessoa.id.toString())}>
            <ListItemAvatar>
              <Avatar>
                <PersonIcon />
              </Avatar>
            </ListItemAvatar>

            <ListItemText>{pessoa.name}</ListItemText>
          </ListItem>
        ))}
      </List>
    </Box>
  )
}
