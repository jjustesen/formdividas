import React from 'react'

import { Box, Divider } from '@material-ui/core'

import { AdminLayoutListEmpty } from './AdminLayoutListEmpty'
import { AdminLayoutListItem } from './AdminLayoutListItem'
import { AdminLayoutListLoading } from './AdminLayoutListLoading'
import { useQueryGetDividas } from '../../api/queries/getDividas'
import { groupBy } from '../../helpers/groupBy'
import { usePerson } from '../../providers/person'

export const AdminLayoutList = () => {
  const { person } = usePerson()

  const {
    data: { result },
    isFetching
  } = useQueryGetDividas()
  const dataDividas = groupBy(result, (user) => user.idUsuario)

  const dividasFiltradas = person ? dataDividas.filter((a) => a.key === person) : dataDividas
  console.log({ person, dividasFiltradas })

  if (isFetching) {
    return <AdminLayoutListLoading />
  }

  if (!dataDividas.length) {
    return <AdminLayoutListEmpty />
  }

  return (
    <Box>
      {dividasFiltradas.map((item) => (
        <>
          <AdminLayoutListItem {...item} />

          <Box my={3}>
            <Divider />
          </Box>
        </>
      ))}
    </Box>
  )
}
