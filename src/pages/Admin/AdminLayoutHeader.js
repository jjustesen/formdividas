import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@material-ui/core'
import { stylesAdminLayoutFilters } from './styles'

import { DialogDividaCreate } from '../Dialog/DialogDividaCreate'

export const AdminLayoutHeader = () => {
  const { t } = useTranslation()
  const classes = stylesAdminLayoutFilters()

  const [open, setOpen] = useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }

  return (
    <Box display="flex" flexDirection="column">
      <Box display="flex" alignItems="center" justifyContent="space-between" marginTop={3}>
        <Box display="flex">
          <Button onClick={handleClickOpen} className={classes.buttonSpacing} color="primary" variant="contained">
            {t('createDebt')}
          </Button>
        </Box>

        <DialogDividaCreate open={open} setOpen={setOpen} />
      </Box>
    </Box>
  )
}
