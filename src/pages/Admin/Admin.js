import { Box } from '@material-ui/core'
import React from 'react'

import { AppBar } from '../../components/AppBar'

import { AdminLayout } from './AdminLayout'

const Admin = () => {
  return (
    <>
      <AppBar />

      <Box marginTop={8}>
        <AdminLayout />
      </Box>
    </>
  )
}

export default Admin
