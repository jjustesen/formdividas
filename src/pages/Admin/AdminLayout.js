import React from 'react'

import { Box } from '@material-ui/core'
import { AdminLayoutHeader } from './AdminLayoutHeader'
import { AdminLayoutList } from './AdminLayoutList'
import { AdminLayoutPeople } from './AdminLayoutPeople'

export const AdminLayout = () => {
  return (
    <Box display="flex" flex={1} minHeight="calc(100vh - 64px)">
      <AdminLayoutPeople />

      <Box display="flex" flexDirection="column" flex={1} margin={3}>
        <Box marginBottom={5}>
          <AdminLayoutHeader />
          <AdminLayoutList />
        </Box>
      </Box>
    </Box>
  )
}
