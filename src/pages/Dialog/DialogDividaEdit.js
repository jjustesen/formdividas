import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import { useQueryGetClientes } from '../../api/queries/getClientes'
import { useFormik } from 'formik'
import * as yup from 'yup'
import { useMutation, useQueryClient } from 'react-query'
import { stylesAdminLayoutFilters } from '../Admin/styles'
import { useQueryGetDividasById } from '../../api/queries/getDividasById'
import { useUser } from '../../helpers/useUser'
import { mutationpostDividaById } from '../../api/mutations/postDividaById'

export const DialogDividaEdit = ({ opened, onClose, onCancel, id }) => {
  const { t } = useTranslation()
  const classes = stylesAdminLayoutFilters()
  const queryClient = useQueryClient()

  const { data: dataUser } = useQueryGetClientes()

  const {
    data: { result }
  } = useQueryGetDividasById({ id })

  const user = useUser(result.idUsuario)

  const validationSchema = yup.object({
    usuario: yup.object().required(t('geral:validacoes.obrigatorio')).required('valor is required'),
    valor: yup.number('Enter your valor').required('valor is required'),
    motivo: yup.string('Enter your motivo').required('motivo is required')
  })

  const { mutate } = useMutation((e) => mutationpostDividaById({ id }, e), {
    onSuccess: () => {
      queryClient.invalidateQueries('getDividas')
    }
  })

  const formik = useFormik({
    initialValues: { usuario: user, valor: result.valor, motivo: result.motivo },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      mutate(
        {
          idUsuario: values.usuario.id,
          valor: values.valor,
          motivo: values.motivo
        },
        {
          onSuccess: () => {
            formik.resetForm({})
          }
        }
      )

      onClose()
    }
  })

  return (
    <Dialog open={opened} onClose={onClose} aria-labelledby="form-dialog-title" fullWidth>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle id="form-dialog-title">{t('newDebt')}</DialogTitle>
        <DialogContent style={{ overflow: 'hidden' }}>
          <FormControl fullWidth className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Pessoa</InputLabel>
            <Select
              value={formik.values.usuario}
              id="usuario"
              name="usuario"
              onChange={formik.handleChange}
              displayEmpty
              className={classes.selectEmpty}
              inputProps={{ 'aria-label': 'Without label' }}
            >
              <MenuItem></MenuItem>
              {dataUser.map((user) => {
                return <MenuItem value={user}>{user.name}</MenuItem>
              })}
            </Select>
          </FormControl>
          <TextField
            fullWidth
            id="valor"
            name="valor"
            label="valor"
            value={formik.values.valor}
            onChange={formik.handleChange}
            error={formik.touched.valor && Boolean(formik.errors.valor)}
            helperText={formik.touched.valor && formik.errors.valor}
          />
          <TextField
            fullWidth
            id="motivo"
            name="motivo"
            label="motivo"
            type="motivo"
            value={formik.values.motivo}
            onChange={formik.handleChange}
            error={formik.touched.motivo && Boolean(formik.errors.motivo)}
            helperText={formik.touched.motivo && formik.errors.motivo}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel}>{t('cancel')}</Button>
          <Button color="primary" type="submit">
            {t('confirm')}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}
