import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { useMutation, useQueryClient } from 'react-query'
import { useSnackbar } from '../../hooks/useSnackbar'
import { mutationDeleteDividaById } from '../../api/mutations/deleteDividaById'

export const DialogDividaDelete = ({ opened, onClose, onCancel, id }) => {
  const { t } = useTranslation()
  const queryClient = useQueryClient()
  const { createSnackbar } = useSnackbar()

  const { mutate } = useMutation((id) => mutationDeleteDividaById({ id }), {
    onSuccess: () => {
      queryClient.invalidateQueries('getDividas')
      createSnackbar('error', t('deletedDebt'))
      onClose()
    }
  })

  return (
    <Dialog open={opened} onClose={onClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{t('delete')}</DialogTitle>
      <DialogContent>{t('deleteDebtMessage')}</DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>{t('cancel')}</Button>
        <Button onClick={() => mutate(id)} color="primary" type="submit">
          {t('confirm')}
        </Button>
      </DialogActions>
    </Dialog>
  )
}
