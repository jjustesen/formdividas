import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import { useQueryGetClientes } from '../../api/queries/getClientes'
import { useFormik } from 'formik'
import * as yup from 'yup'
import { mutationPostDivida } from '../../api/mutations/postDivida'
import { useMutation, useQueryClient } from 'react-query'
import { stylesAdminLayoutFilters } from '../Admin/styles'
import { useSnackbar } from '../../hooks/useSnackbar'

export const DialogDividaCreate = ({ open, setOpen }) => {
  const { t } = useTranslation()
  const classes = stylesAdminLayoutFilters()
  const queryClient = useQueryClient()
  const { createSnackbar } = useSnackbar()

  const validationSchema = yup.object({
    usuario: yup.object().required(t('geral:validacoes.obrigatorio')).required('valor is required'),
    valor: yup.number('Enter your valor').required('valor is required'),
    motivo: yup.string('Enter your motivo').required('motivo is required')
  })

  const { data: dataUser } = useQueryGetClientes()

  const handleClose = () => {
    setOpen(false)
  }

  const { mutate } = useMutation((e) => mutationPostDivida(e), {
    onSuccess: () => {
      queryClient.invalidateQueries('getDividas')
      createSnackbar('success', t('registeredDebt'))
    }
  })

  const formik = useFormik({
    initialValues: {},
    validationSchema: validationSchema,
    onSubmit: (values) => {
      mutate(
        {
          idUsuario: values.usuario.id,
          valor: values.valor,
          motivo: values.motivo
        },
        {
          onSuccess: () => {
            formik.resetForm({})
          }
        }
      )

      handleClose()
    }
  })

  return (
    <Dialog open={open} onClose={handleClose} fullWidth>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle id="form-dialog-title">{t('newDebt')}</DialogTitle>
        <DialogContent style={{ overflow: 'hidden' }}>
          <FormControl fullWidth className={classes.formControl}>
            <InputLabel>{t('people')}</InputLabel>
            <Select
              value={formik.values.user}
              id="usuario"
              name="usuario"
              onChange={formik.handleChange}
              displayEmpty
              className={classes.selectEmpty}
              inputProps={{ 'aria-label': 'Without label' }}
            >
              <MenuItem></MenuItem>
              {dataUser.map((user) => {
                return <MenuItem value={user}>{user.name}</MenuItem>
              })}
            </Select>
          </FormControl>
          <TextField
            fullWidth
            id="valor"
            name="valor"
            label={t('value')}
            value={formik.values.valor}
            onChange={formik.handleChange}
            error={formik.touched.valor && Boolean(formik.errors.valor)}
            helperText={formik.touched.valor && formik.errors.valor}
          />
          <TextField
            fullWidth
            id="motivo"
            name="motivo"
            label={t('reason')}
            type="motivo"
            value={formik.values.motivo}
            onChange={formik.handleChange}
            error={formik.touched.motivo && Boolean(formik.errors.motivo)}
            helperText={formik.touched.motivo && formik.errors.motivo}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>{t('cancel')}</Button>
          <Button color="primary" type="submit">
            {t('confirm')}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}
