import moment from 'moment'

export const formatDate = (dateTime) => {
  if (moment(dateTime).isValid) {
    if (moment(dateTime).isSame(moment(), 'day')) {
      return moment().subtract(10, 'days').calendar()
    }

    return moment(dateTime).format('L')
  }

  return '-'
}
