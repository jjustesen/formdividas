import { useQuery } from 'react-query'
require('dotenv/config')

export const queryGetDividasById = ({ id }) => {
  let url = `${process.env.REACT_APP_API_URL}/${id}/?uuid=${process.env.REACT_APP_API_KEY}`

  return fetch(url).then((resp) => resp.json())
}

export const useQueryGetDividasById = (pathParams, queryParams, config) =>
  useQuery('getDividasById', () => queryGetDividasById(pathParams, queryParams), config)
useQueryGetDividasById.queryKey = 'getDividasById'
