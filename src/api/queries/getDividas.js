import { useQuery } from 'react-query'
require('dotenv/config')

export const queryGetDividas = () => {
  let url = `${process.env.REACT_APP_API_URL}/?uuid=${process.env.REACT_APP_API_KEY}`
  return fetch(url).then((resp) => resp.json())
}

export const useQueryGetDividas = (queryParams, config) => {
  return useQuery('getDividas', () => queryGetDividas(queryParams), config)
}
useQueryGetDividas.queryKey = 'getDividas'
