import { useQuery } from 'react-query'
require('dotenv/config')

export const queryGetClientes = () => {
  return fetch(process.env.REACT_APP_API_USUARIOS_URL).then((resp) => resp.json())
}

export const useQueryGetClientes = (queryParams, config) =>
  useQuery('getClientes', () => queryGetClientes(queryParams), config)
useQueryGetClientes.queryKey = 'getClientes'
