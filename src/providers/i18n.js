import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

import { getLanguage } from '../services/language'

const resources = {
  'en-US': {
    translation: {
      app_title: 'Desafio Dividas',
      help: 'Help',
      settings: 'Settings',
      exit: 'Exit',
      search: 'Search',
      confirm: 'Confirm',
      cancel: 'Cancel',
      new: 'New',
      switchTheme: 'Switch theme',
      changeLanguage: 'Change language',
      language: 'Language',
      'pt-BR': 'Brazilian Portuguese',
      'en-US': 'English',
      'es-ES': 'Spanish',

      noItemsFound: 'No items found!',

      createDebt: 'Create Debt',
      newDebt: 'New debt',
      name: 'Name',
      reason: 'Reason',
      value: 'Value',
      date: 'Date',

      deleteDebtMessage: 'Would you like to delete this debt?',
      registeredDebt: 'Debt successfully registered!',
      deletedDebt: 'Successfully Deleted!',
      updatedDebt: 'Successfully updated!',
      people: 'People',
      all: 'All'
    }
  },
  'es-ES': {
    translation: {
      app_title: 'Desafio Dividas',
      help: 'Ayuda',
      settings: 'Configuraciones',
      exit: 'Salir',
      search: 'Buscar',
      confirm: 'Confirmar',
      cancel: 'Cancelar',
      new: 'Nuevo',
      switchTheme: 'Cambiar tema',
      changeLanguage: 'Cambiar idioma',
      language: 'Idioma',
      'pt-BR': 'Portugués de Brasil',
      'en-US': 'Inglés',
      'es-ES': 'Español',

      noItemsFound: 'No se encontraron artículos!',

      createDebt: 'Registrar deuda',
      newDebt: 'Nueva deuda',
      name: 'Nombre',
      reason: 'Razón',
      value: 'Valor',
      date: 'Fecha',

      deleteDebtMessage: '¿Desea eliminar esta deuda?',
      registeredDebt: '¡Registrado exitosamente!',
      deletedDebt: '¡Eliminado con éxito!',
      updatedDebt: '¡Actualizado con éxito!',
      people: 'Gente',
      all: 'Todo'
    }
  },
  'pt-BR': {
    translation: {
      app_title: 'Desafio Dividas',
      help: 'Ajuda',
      settings: 'Configurações',
      exit: 'Sair',
      search: 'Pesquisar',
      confirm: 'Confirmar',
      cancel: 'Cancelar',
      delete: 'Deletar',
      new: 'Novo',
      switchTheme: 'Alternar tema',
      changeLanguage: 'Alterar linguagem',
      language: 'Linguagem',
      'pt-BR': 'Português do Brasil',
      'en-US': 'Inglês',
      'es-ES': 'Espanhol',

      noItemsFound: 'Nenhum item encontrado!',

      createDebt: 'Cadastrar divida',
      newDebt: 'Nova divida',
      name: 'Nome',
      reason: 'Razão',
      value: 'Valor',
      date: 'Data',

      deleteDebtMessage: 'Você deseja deletar esta divida?',
      registeredDebt: 'Cadastrado com sucesso!',
      deletedDebt: 'Deletado com sucesso!',
      updatedDebt: 'Atualizado com sucesso!',
      people: 'Pessoas',
      all: 'Todos'
    }
  }
}

i18n.use(initReactI18next).init({
  resources,
  lng: getLanguage() || 'pt-BR',

  keySeparator: false,

  interpolation: {
    escapeValue: false
  }
})

export default i18n
