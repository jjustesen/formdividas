import React, { Suspense } from 'react'

import 'moment/locale/pt-br'
import 'moment/locale/es'

import { I18nextProvider } from 'react-i18next'
import { SnackbarProvider } from 'react-snackbar-alert'

import i18n from './i18n'
import Theme from './theme'

import { ReactQueryProvider } from './reactQuery'
import PersonProvider from './person'

const Providers = ({ children }) => {
  return (
    <Theme>
      <PersonProvider>
        <I18nextProvider i18n={i18n}>
          <SnackbarProvider timeout={3000} sticky={false} dismissable={false} position="bottom-left">
            <ReactQueryProvider>
              <Suspense fallback={() => <p>Carregando...</p>}>{children}</Suspense>
            </ReactQueryProvider>
          </SnackbarProvider>
        </I18nextProvider>
      </PersonProvider>
    </Theme>
  )
}

export default Providers
